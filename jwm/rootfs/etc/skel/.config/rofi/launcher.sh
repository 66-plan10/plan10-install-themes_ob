#!/usr/bin/bash

## Author  : Aditya Shakya
## Mail    : adi1090x@gmail.com
## Github  : @adi1090x
## Twitter : @adi1090x
## Modified by <eric@obarun.org> for Obarun

rofi -no-config -no-lazy-grab -show drun -modi drun -theme ~/.config/rofi/color/launcher.rasi
